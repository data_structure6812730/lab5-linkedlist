class Link {
    public int iData;      // data item (key)
    public double dData;   // data item
    public Link next;      // next link in the list

    // Constructor
    public Link(int id, double dd) {
        iData = id;         // initialize data
        dData = dd;         // 'next' is automatically set to null
    }

    // Display link's data
    public void displayLink() {
        System.out.print("{" + iData + ", " + dData + "} ");
    }
}

class LinkList {
    private Link first;     // reference to the first link in the list

    // Constructor
    public LinkList() {
        first = null;        // no items in the list yet
    }

    // Check if the list is empty
    public boolean isEmpty() {
        return (first == null);
    }

    // Insert at the start of the list
    public void insertFirst(int id, double dd) {
        Link newLink = new Link(id, dd);    // create a new link
        newLink.next = first;               // newLink --> old first
        first = newLink;                   // first --> newLink
    }

    // Delete the first item in the list (assumes the list is not empty)
    public Link deleteFirst() {
        Link temp = first;           // save reference to the first link
        first = first.next;         // delete it: first --> old next
        return temp;                // return the deleted link
    }

    // Display the entire list
    public void displayList() {
        System.out.print("List (first --> last): ");
        Link current = first;       // start at the beginning of the list
        while (current != null) {   // until the end of the list
            current.displayLink();  // print data
            current = current.next; // move to the next link
        }
        System.out.println();
    }
}

class LinkListApp {
    public static void main(String[] args) {
        LinkList theList = new LinkList();   // create a new list
        theList.insertFirst(22, 2.99);       // insert four items
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);
        theList.displayList();               // display the list

        while (!theList.isEmpty()) {        // until it's empty
            Link aLink = theList.deleteFirst(); // delete a link
            System.out.print("Deleted ");     // display the deleted link
            aLink.displayLink();
            System.out.println();
        }

        theList.displayList();               // display the empty list
    }
}
